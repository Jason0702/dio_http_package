// 自訂義異常
import 'dart:convert';
import 'dart:developer';

import 'package:dio/dio.dart';
import 'package:dio_http_package/model/error_model.dart';

class AppException implements Exception {
  final String _message;
  final int _code;

  AppException(
      this._code,
      this._message,
      );

  @override
  String toString() {
    log("$_code$_message");
    return _message;
  }

  String getMessage() {
    return _message;
  }
  factory AppException.create(DioError error) {
    switch (error.type) {
      case DioErrorType.cancel:
        {
          log("-1,請求取消");
          return BadRequestException(-1, "資料連線下載有誤，請與客服人員聯繫");
        }
      case DioErrorType.connectTimeout:
        {
          log("-1,連接超時");
          return BadRequestException(-1, "資料連線下載有誤，請與客服人員聯繫");
        }
      case DioErrorType.sendTimeout:
        {
          log("-1,請求超時");
          return BadRequestException(-1, "資料連線下載有誤，請與客服人員聯繫");
        }
      case DioErrorType.receiveTimeout:
        {
          log("-1,響應超時");
          return BadRequestException(-1, "資料連線下載有誤，請與客服人員聯繫");
        }
      case DioErrorType.response:
        {
          try {
            int? errCode = error.response!.statusCode;
            ErrorModel errorModel = ErrorModel.fromJson(json.decode(json.encode(error.response!.data)));
            String? errMsg = errorModel.message;
            //log('Error: ${error.message}');
            //return AppException(errCode!, errMsg!);
            switch (errCode) {
              case 400:
                {
                  return BadRequestException(errCode!, "$errMsg");
                }
              case 401:
                {
                  return BadRequestException(errCode!, "$errMsg");
                }
              case 403:
                {
                  return UnauthorisedException(errCode!, "$errMsg");
                }
              case 404:
                {
                  return UnauthorisedException(errCode!, "$errMsg");
                }
              case 405:
                {
                  return UnauthorisedException(errCode!, "$errMsg");
                }
              case 500:
                {
                  return UnauthorisedException(errCode!, "$errMsg");
                }
              case 502:
                {
                  return UnauthorisedException(errCode!, "$errMsg");
                }
              case 503:
                {
                  return UnauthorisedException(errCode!, "$errMsg");
                }
              case 505:
                {
                  return UnauthorisedException(errCode!, "$errMsg");
                }
              default:
                {
                  return AppException(errCode!, errMsg!);
                }
            }
          } on Exception catch (_) {
            return AppException(-1, "未知錯誤");
          }
        }
      default:
        {
          return AppException(-1, error.error.message);
        }
    }
  }
}

/// 請求錯誤
class BadRequestException extends AppException {
  BadRequestException(int code, String message) : super(code, message);
}

/// 未認證異常
class UnauthorisedException extends AppException {
  UnauthorisedException(int code, String message) : super(code, message);
}